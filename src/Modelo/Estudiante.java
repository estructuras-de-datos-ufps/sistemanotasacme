/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author madarme
 */
public class Estudiante {
    private long codigo;
    private float notas[];

    public Estudiante() {
    }

    public Estudiante(long codigo) {
        this.codigo = codigo;
    }

    public Estudiante(String datos[])
    {
    this.codigo=Long.parseLong(datos[0]);
    this.notas=new float[datos.length-1];
    for(int i=1;i<datos.length;i++)
        this.notas[i-1]=Float.parseFloat(datos[i]);
    }
    
    
    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public float[] getNotas() {
        return notas;
    }

    public void setNotas(float[] notas) {
        this.notas = notas;
    }
    
    
    public String toString()
    {
    String msg="\nCodigo:"+this.codigo+"->";
    for(float dato:this.notas)
        msg+=dato+",";
    
    return msg;
    }
    
    
}
