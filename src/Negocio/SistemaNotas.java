/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;

/**
 *
 * @author madarme
 */
public class SistemaNotas {
    
    
    private Estudiante estudiantes[];

    public SistemaNotas() {
    }
    
    
    public void cargarNotas(String info)
    {
    
        String m[]=info.split("\n");
        this.estudiantes=new Estudiante[m.length];
        
        
        for(int i=0;i<m.length;i++)
        {
        String datosEstudiante[]=m[i].split(";");
        this.estudiantes[i]=new Estudiante(datosEstudiante);
        }
            
            
    
    }
    
    
    
    public String listarEstudiantes()
    {
     String msg="";
    for(Estudiante dato:this.estudiantes)
    {
        msg+=dato.toString();
        
    }    
    
    return msg;
    }
}
